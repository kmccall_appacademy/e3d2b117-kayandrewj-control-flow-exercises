def destructive_uppercase(str)
  arr = str.split("")
  arr.each_index do |i|
    next if arr[i] != arr[i].downcase
    arr[i] = nil
  end
  arr.join("")
end

def middle_substring(str)
  str[(-1 - str.length/2)..(str.length/2)]
end

VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.split("").each do |char|
    next if !VOWELS.include?(char)
    count += 1
  end
  count
end

def factorial(num)
  (1..num).reduce(:*)
end

def my_join(arr, separator = "")
  joined_str = ""
  arr.each do |char|
    joined_str += "#{char}#{separator}"
  end
  if joined_str[-1] == separator
    joined_str.slice!(-1)
  end
  joined_str
end

def weirdcase(str)
  char_arr = str.downcase.split("")
  char_arr.each_index do |i|
    next if i%2 == 0
    char_arr[i].upcase!
  end
  char_arr.join
end

def reverse_five(str)
  word_arr = str.split(" ")
  word_arr.each_index do |i|
    word = word_arr[i]
    next if word.length < 5
    word_arr[i] = word.reverse
  end
  word_arr.join(" ")
end

def fizzbuzz(n)
  ints =*(1..n)
  ints.each_index do |i|
    num = i + 1
    if num%3 == 0 && num%5 == 0
      ints[i] = "fizzbuzz"
    elsif num%5 == 0
      ints[i] = "buzz"
    elsif num%3 == 0
      ints[i] = "fizz"
    end
  end
  ints
end

def my_reverse(arr)
  reversed_arr = []
  arr.length.times do
    reversed_arr << arr.pop
  end
  reversed_arr
end

def prime?(num)
  return false if num == 1
  arr =*(2..(num/(Math.sqrt(num))))
  arr.each do |x|
    if num%x == 0
      return false
    end
  end
  true
end

def factors(num)
  factor_arr = [1, num]
  (num-2).times do |i|      # I had to do this weird thing to avoid dividing by 0
    i = i+2                 # and to skip 1. Note: is there something like
    next if num%i != 0      #`drop(1)` for .times or .each_index?
    factor_arr << i
  end
  factor_arr.sort
end

def prime_factors(num)
  prime_f_arr = []
  all_factors = factors(num)
  all_factors.map do |factor|
    prime_f_arr << factor if prime?(factor)
  end
  prime_f_arr
end

def num_prime_factors(num)
  prime_factors(num).length
end

def oddball(arr)
  evens = arr.select { |num| num%2 == 0}
  odds = arr.select { |num| num%2 != 0}
  evens.length == 1 ? evens[0] : odds[0]
end
